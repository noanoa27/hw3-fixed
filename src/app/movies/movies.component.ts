import { Component, OnInit } from '@angular/core';

/*
export interface PeriodicElement {
  id:number;
  title:string;
  studio:string;
  weekendIncome:string;
 
}
*/

  @Component({
    selector: 'movies',
    templateUrl: './movies.component.html',
    styleUrls: ['./movies.component.css']
  })

  export class MoviesComponent implements OnInit {


    name= "no movie";
    movies /* ELEMENT_DATA: PeriodicElement[] */ = [ 
      {"id":1,"title":"Spider-Man: Into The Spider-Verse","studio":"Sony","weekendIncome":"$35,400,000"},
      {"id":2,"title":"The Mule","studio":"WB","weekendIncome":"$17,210,000"},
      {"id":3,"title":"Dr. Seuss' The Grinch (2018)","studio":"Uni.","weekendIncome":"$11,580,000"},
      {"id":4,"title":"Ralph Breaks the Internet","studio":"BV","weekendIncome":"$9,589,000"},
      {"id":5,"title":"Mortal Engines","studio":"Uni.","weekendIncome":"$7,501,000"}
    ];
  
  displayedColumns: string[] = ['id', 'title', 'studio', 'weekendIncome','delete'];
 // dataSource = ELEMENT_DATA;
  toDelete(element)
  {
    let x,y = this.movies;
    let z = element.id;
 
    if (this.movies.length == 1)
    {
      this.movies = [];
    }
    if (z > this.movies.length)
    {
      z = this.movies.length-1;
    }
    
    x = this.movies.slice(0,z-1);
    y = this.movies.slice(z, this.movies.length);
    this.movies = x;
    this.movies = this.movies.concat(y);
    this.name = element.title;
   
  }
 
  
  constructor() { }

  ngOnInit() {
  }

}
